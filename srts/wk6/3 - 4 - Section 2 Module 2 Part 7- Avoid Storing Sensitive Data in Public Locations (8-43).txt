[MUSIC].
Let's take a look at some of the ways that
we can write apps that accidentally leak
data to other malicious apps on a device
by
not properly securing our interactions
with the storage system.
Now as we've seen, as is in most cases, we
have a
process boundary which protects our app
and its memory, from another app.
And also as we've discussed, we have an
area,
within the storage system, that by
default, whenever our
app saves data, allows us to save it
privately
and prevent other apps from getting access
to it.
But, there are certain types of
interactions with the
file system that, if our app uses those
interactions,
it will end up storing data in a way
that it's accessible to other apps on the
device.
So, there are cases where we can store
data, and think that we're storing it
securely.
But in reality, we're opening up the
ability for malware to
get access to that data and then steal it
from our application.
So we always want to make sure that
whenever
we store private data on Android, that we
do
it properly, and we don't do it in a
way that allows other applications to gain
access to it.
So let's take a look at one particular way
that we can
accidentally leak data from our app to
other apps on the device.
And this is actually quite a common way
that developers make mistakes
and end up exposing important settings or
private user data to other apps.
So the way that, developers often make a
mistake is they
assume that data that they're storing to
the SD Card is secure.
Or they assume that other apps aren't
going to look at the data, or
know to look for the data that their app
is saving on the SD Card.
And this is actually not true.
If you store data on the SD card, it's
actually public to all other apps on a
device.
So whenever you see code that has get
external storage directory, you need to be
very careful that you ensure that nothing
private
is being stored to that external storage
directory.
And the reason is, is that the external
storage directory
is typically the SD card, but it is almost
always a
public area for storing information that
any app can potentially
get access to regardless of what user
account saved the data there.
So just because your app saves data there
and has a separate user account from
another
app it doesn't mean that that data that's
stored on the external storage directory
is actually secure.
So what you want to look for is the
creation, for example, in this case
of a file that's being stored with
the parent directory being the external
storage directory.
So in this case, the user that's logging
in has a username and password, and
the developer is actually taking that
password
and storing it on the external storage
directory.
And then we see, finally, they write the
actual password to
the external storage directory, and
there's a whole host of issues
with storing passwords on disk, but, even
if this wasn't a
password, it was some other secure
setting, it's still a problem.
And the reason is is because we've gone
from secure data to the external storage
directory, and
then written that data there, assuming
that it
was private in some way, when actually it
isn't.
As I mentioned a second ago, you don't
really want to store passwords on disk.
If you can come up with a design
for your application that doesn't require
storing passwords.
If you can use an, token authentication
scheme, where you get
tokens from a server, or force the user to
log in each
time, in order to gain access to critical
settings or private settings,
you're probably much better off than if
you're storing passwords on disk.
It's very difficult to store something
sensitive like a password
on disk and do it in a very secure manner.
You have to know exactly what you're doing
and be very, very careful with this.
If you absolutely have to store something
sensitive,
like a password, on disk, you really need
to
understand how to encrypt that data
properly or
how to hash it properly using something
like VCrypt.
And, you don't want to do this unless you
absolutely understand what you're doing.
You don't want to develop your own
homegrown encryption scheme for storing
stuff to disk.
And now there have been cases of apps
where people
have taken things like Google protocol
buffers, which encode data into
a binary format that isn't necessarily
human readable and they've stored
data in that format assuming that it was
secure on disk.
But the reality is, is that even something
like Google protocol buffrs isn't secure.
It's not a real security mechanism.
If you're going to actually store
something on
disk, you need to do it with a
real strong well known well studied
encryption algorithm
and you don't need to build your own
implementation.
You need to use an existing
implementation.
The actual algorithms and processes for
encrypting data and storing it on disc are
beyond the scope of this MOOC, but I
encourage those who are out there and
are going to write applications that store
data of this sensitivity on disk, to
go and look at the available encryption
options for storing data on disk on
Android.
Regardless of if you're encrypting data or
not, anything
private should never get stored on the
external storage directory.
There is no reason to take data that's
private, even if it's encrypted,
and possibly expose it to other
applications by storing it on the SD card.
If you see direct data that's going to the
external
storage directory, check and say is this
data something that's public.
Do I want every other app to read it?
And if there's any case where you say I
don't want another
app to read it, then you shouldn't be
storing it on external storage.
You must approach external storage with
the mentality that
anything you store there will be read by
other applications.
And they will be able to interpret that
data,
and they will be able to do things with
it.
If you take that mentality, then it makes
it easier to make a
decision about what goes on external
storage versus doesn't go on external
storage.
And to make decisions that don't end up
exposing
user data or exposing private settings
from your application.
So what would be an acceptable use of
external storage?
Well, one common acceptable use of
external storage is to store photos.
So, for example, if you have a
custom camera application, you probably
want to store those
photos to the SD card, so that the user
can use their phone like a camera.
Pop in an SD card.
Take a bunch of photos.
Store them to the SD card, and then pull
that SD card out, and put
a new SD card in when the SD card that
they'd been using before became full.
So, in that case it probably does make
sense
to store those photos on the external
storage directory.
Also, if they're stored on the external
storage directory, it can allow
things like the Gallery Application to
more easily get access to them.
So, there are cases where we want to store
to external storage.
But we have to remember that we can't just
assume that because we're taking a
photo or we're taking some other type of
data that it's necessarily appropriate for
external storage.
So, for example, although we might have an
application where we're actually storing
something that is
benign, maybe it really is a innocuous
photo
that we're storing on the actual SD card.
It's not going to be the case that it
always
make sense to store photos on the SD card.
So if you're building Snapchat you don't
want to store photos on the SD card.
Because the whole purpose is, is that
photos are private and can't be seen.
So, you need to be very careful and
evaluate whether or not it makes
sense to store data on the external
storage or SD card of the device.

