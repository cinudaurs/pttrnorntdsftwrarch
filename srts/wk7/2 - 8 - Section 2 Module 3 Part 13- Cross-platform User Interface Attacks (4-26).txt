[MUSIC]
Malware developers will go to great
lengths
to try to get malware onto your device.
So one of the user manipulation attacks
that they've started to employ.
Is to use your trust in different devices
that you
already own in order to get you to install
malware.
So one of the most sophisticated versions
of these attacks is through a technique
that's called cross-platform malware.
And what these attacks do is, they first
get you
to, using various means, install some
malware
on your traditional computing equipment.
Something like a laptop here.
And then, what they do is they wait for
you to go.
And visit some website or other location
that you trust.
So for example lets say that you go and
browse in your web browser to your bank.
And when you get the your bank the malware
on
your laptop identifies that you have just
browsed your banking website.
And they use that opportunity and the
trust that you have with your
bank to try to trick you into installing
malware on your mobile device.
And the common way that they do it is,
they'll wait until you visit your bank.
And then, they'll display a QR code
telling
you that if you'll scan this QR code.
You can install the bank's app and manage
your account remotely.
Now, as a user you feel like this must be
a legitimate
request, to install that bank's app,
because you just browsed your banking
website.
But what you don't realize is that this
malware is lurking on your device.
Waiting for you to go to that website, so
that it can display
that fake message to you at exactly the
moment that you'll trust it most.
So it'll display that fake message to you
right when you visit your bank's website.
When you'll be more trusting of the
message to install an arbitrary app.
So they'll display a QR code that links to
the
app store of your choice to download that
particular malware.
And then that malware will then propagate
to your phone.
And now, at this point, the attacker has a
very powerful position.
They have both your traditional computing
environment, like your
laptop, infected and they have your mobile
phone infected.
And your Android phone now has malware on
it
that they can use to help perform complex
attacks.
One of the complex attacks that's been
seen in the
wild is to go and initiate a fraudulent
banking transaction.
To take money from your account and
deposit it, in the attackers account.
And a lot of banks, what they'll do is
they'll
require that if you're going to initiate
some large transfer.
That you have an SMS sent to your device
that has a code in it.
That you need to type into the website in
order to
authorize and prove that this transaction
is really coming from you.
And now what they can do is if they've got
you
to install the malware on both your device
and your laptop.
When that SMS is sent to your device, the
malware can receive that code.
Forward it back to your device, your
laptop or other device,
in order to be fed into the website, and
complete the transaction.
So, malware isn't just about gaining
access to one device, but it
may be access to gaining multiple devices
on it that you own.
And using the trust that you have in each
of
the individual devices to further its
spread across your other devices.
And then, using the trust that other
entities like your
bank have in your mobile device to
complete fraudulent activity

