
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @class SimpleSemaphore
 *
 * @brief This class provides a simple counting semaphore
 *        implementation using Java a ReentrantLock and a
 *        ConditionObject.  It must implement both "Fair" and
 *        "NonFair" semaphore semantics, just liked Java Semaphores.
 */
public class SimpleSemaphore {


    /**
     * Define a count of the number of available permits.
     */
    private int maxPermits;

    private boolean fair=true;


    /**
     * Constructor initialize the data members.
     **/
    public SimpleSemaphore (int permits,
                            boolean fair)
    {
        this.maxPermits = permits;
        this.fair = fair;

    }

    /**
     * Acquire one permit from the semaphore in a manner that can
     * be interrupted.
     */
    public void acquire() throws InterruptedException {

        permitLock.lock();

        try {
            while (maxPermits == 0) {
                try {
                    permitsNotZero.await();  //wait till permits becomes non zero
                } catch (InterruptedException e) { }
            }
            maxPermits --;

        } finally {
            permitLock.unlock();
        }
    }

    /**
     * Acquire one permit from the semaphore in a manner that
     * cannot be interrupted.
     */
    public void acquireUninterruptibly() {

        permitLock.lock();

        try{
            while ( maxPermits == 0) {
                try {
                    permitsNotZero.awaitUninterruptibly();  //wait till permits becomes non zero
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            maxPermits --;

        } finally {
            permitLock.unlock();
        }

    }

    /**
     * Return one permit to the semaphore.
     */
    public void release() {

        permitLock.lock();

        try{

            maxPermits ++;
            permitsNotZero.signalAll();


        }finally {
            permitLock.unlock();
        }



    }



    /**
     * Define a ReentrantLock to protect the critical section.
     */
    private Lock permitLock = new ReentrantLock(fair);

    /**
     * Define a ConditionObject to wait while the number of
     * permits is 0.
     */
    private Condition permitsNotZero = permitLock.newCondition();



}

