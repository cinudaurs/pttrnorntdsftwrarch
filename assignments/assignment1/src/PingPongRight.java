// Import the necessary Java synchronization and scheduling classes.

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

/**
 * @class PingPongRight
 *
 * @brief This class implements a Java program that creates two
 *        instances of the PlayPingPongThread and start these thread
 *        instances to correctly alternate printing "Ping" and "Pong",
 *        respectively, on the console display.
 */
public class PingPongRight {
    /**
     * Number of iterations to run the test program.
     */
    public static int mMaxIterations = 10;


    
    /**
     * Latch that will be decremented each time a thread exits.
     */
    public static CountDownLatch mStopLatch = new CountDownLatch(2);


    /**
     * @class PlayPingPongThread
     *
     * @brief This class implements the ping/pong processing algorithm
     *         using the SimpleSemaphore to alternate printing "ping"
     *         and "pong" to the console display.
     */
    public static class PlayPingPongThread extends Thread
    {

        /**
         * String to print (either "ping!" or "pong"!) for each
         * iteration.
         */

        private String message = null;

        private CountDownLatch latch = null ;

        SimpleSemaphore pingSema = null;
        SimpleSemaphore pongSema = null;

        final static CyclicBarrier mStartBarrier = new CyclicBarrier(2);

        /**
         * The two SimpleSemaphores use to alternate pings and pongs.
         */

        private SimpleSemaphore acquireSemaphore;
        private SimpleSemaphore releaseSemaphore;


        /**
         * Constructor initializes the data member.
         */
        public PlayPingPongThread (String messg, CountDownLatch latch, SimpleSemaphore acquireSemaphore, SimpleSemaphore releaseSemaphore)
        {

            this.message = messg;
            this.latch = latch;
            this.acquireSemaphore =  acquireSemaphore;
            this.releaseSemaphore = releaseSemaphore;


        }

        /**
         * Main event loop that runs in a separate thread of control
         * and performs the ping/pong algorithm using the
         * SimpleSemaphores.
         */
        public void run ()
        {

           for (int i = 1 ; i <= mMaxIterations ; i++) {

               try {
                   acquireSemaphore.acquire();
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }


               System.out.println(this.message + "(" + i + ")");



               try {
                   Thread.sleep(10);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }

               releaseSemaphore.release();

           }

            latch.countDown();

        }
    }



    /**
     * The main() entry point method into PingPongRight program. 
     */
    public static void main(String[] args) {


        final CountDownLatch latch = new CountDownLatch(2);

        // Create the ping and pong SimpleSemaphores that control
        // alternation between threads.

        SimpleSemaphore pingSema = new SimpleSemaphore(1, true);
        SimpleSemaphore pongSema = new SimpleSemaphore(0, true);


        System.out.println("Ready...Set...Go!");

        // Create the ping and pong threads, passing in the string
        // to print and the appropriate SimpleSemaphores.



        PlayPingPongThread ping =
            new PlayPingPongThread("Ping", latch, pingSema, pongSema);
        PlayPingPongThread pong =
            new PlayPingPongThread("Pong", latch, pongSema, pingSema);

        // Initiate the ping and pong threads, which will call the
        // run() hook method.
        ping.start();
        pong.start();

        // Use barrier synchronization to wait for both threads to
        // finish.

        // TODO - replace replace the following line with a
        // CountDownLatch barrier synchronizer call that waits for
        // both threads to finish.

        try{
            latch.await();  //main thread is waiting on CountDownLatch to finish
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }


        System.out.println("Done!");
    }
}
