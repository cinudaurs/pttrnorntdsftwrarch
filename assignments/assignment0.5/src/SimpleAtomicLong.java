// Import the necessary Java synchronization and scheduling classes.

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @class SimpleAtomicLong
 *
 * @brief This class implements a subset of the
 *        java.util.concurrent.atomic.SimpleAtomicLong class using a
 *        ReentrantReadWriteLock to illustrate how they work.
 */
class SimpleAtomicLong
{
    /**
     * The value that's manipulated atomically via the methods.
     */
    private long mValue;

    /**
     * The ReentrantReadWriteLock used to serialize access to mValue.
     */

    private final ReentrantReadWriteLock mRWLock = new ReentrantReadWriteLock();
    private final Lock r = mRWLock.readLock();
    private final Lock w = mRWLock.writeLock();


    /**
     * Creates a new SimpleAtomicLong with the given initial value.
     */
    public SimpleAtomicLong(long initialValue)
    {

        mValue = initialValue;
    }

    /**
     * @brief Gets the current value.
     *
     * @returns The current value
     */
    public long get()
    {

        r.lock();
        try{
            return mValue;
        }finally{
            r.unlock();
        }
    }

    /**
     * @brief Atomically decrements by one the current value
     *
     * @returns the updated value
     */
    public long decrementAndGet()
    {

        w.lock();
        try{
            mValue--;
            return mValue;

        }finally{
            w.unlock();
        }

    }

    /**
     * @brief Atomically increments by one the current value
     *
     * @returns the previous value
     */
    public long getAndIncrement()
    {

        w.lock();
        try{

            mValue++;

        }finally{
            w.unlock();
        }
        return mValue;
    }

    /**
     * @brief Atomically decrements by one the current value
     *
     * @returns the previous value
     */
    public long getAndDecrement()
    {
        w.lock();
        try{

            mValue--;


        }finally{
            w.unlock();
        }
        return mValue;
    }

    /**
     * @brief Atomically increments by one the current value
     *
     * @returns the updated value
     */
    public long incrementAndGet()
    {
        w.lock();
        try{

            mValue++;

        }finally{
            w.unlock();
        }
        return mValue;
    }
}


