package edu.vuum.mocca;

/**
 * @class edu.vuum.mocca.Palantir
 *
 * @brief Provides an interface for gazing into a edu.vuum.mocca.Palantir.
 *        Essentially plays the role of a "command" in the Command
 *        pattern.
 */
interface Palantir {
    /**
     * Gaze into the edu.vuum.mocca.Palantir (and go into a tranc ;-)).
     */
    public void gaze();

    /**
     * Return the name of the edu.vuum.mocca.Palantir.
     */
    public String name();
}

