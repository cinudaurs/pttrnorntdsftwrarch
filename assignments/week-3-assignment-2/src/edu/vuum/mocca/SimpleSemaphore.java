package edu.vuum.mocca;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @class SimpleSemaphore
 *
 * @brief This class provides a simple counting semaphore
 *        implementation using Java a ReentrantLock and a
 *        ConditionObject.  It must implement both "Fair" and
 *        "NonFair" semaphore semantics, just liked Java Semaphores.
 */
public class SimpleSemaphore {
    /**
     * Constructor initialize the data members.
     *
     */
    private boolean fair=true;



    public SimpleSemaphore (int permits,
                            boolean fair)
    {
        // TODO - you fill in here
        this.maxPermits = permits;
        this.fair = fair;
    }

    /**
     * Acquire one permit from the semaphore in a manner that can
     * be interrupted.
     */
    public void acquire() throws InterruptedException {
        // TODO - you fill in here

        permitLock.lock();

        try {
            while (maxPermits == 0) {
                try {
                    permitsNotZero.await();  //wait till permits becomes non zero
                } catch (InterruptedException e) { }
            }
            maxPermits --;

        } finally {
            permitLock.unlock();
        }


    }

    /**
     * Acquire one permit from the semaphore in a manner that
     * cannot be interrupted.
     */
    public void acquireUninterruptibly() {
        // TODO - you fill in here
        permitLock.lock();

        try{
            while ( maxPermits == 0) {
                try {
                    permitsNotZero.awaitUninterruptibly();  //wait till permits becomes non zero
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            maxPermits --;

        } finally {
            permitLock.unlock();
        }



    }

    /**
     * Return one permit to the semaphore.
     */
    void release() {
        // TODO - you fill in here
        permitLock.lock();

        try{

            maxPermits ++;
            permitsNotZero.signalAll();


        }finally {
            permitLock.unlock();
        }




    }

    /**
     * Define a ReentrantLock to protect the critical section.
     */
    // TODO - you fill in here
    private Lock permitLock = new ReentrantLock(fair);

    /**
     * Define a ConditionObject to wait while the number of
     * permits is 0.
     */
    // TODO - you fill in here

    private Condition permitsNotZero = permitLock.newCondition();

    /**
     * Define a count of the number of available permits.
     */
    // TODO - you fill in here
    private int maxPermits;

    public int availablePermits() {
        return maxPermits;
    }
}

